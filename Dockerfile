FROM ubuntu AS downloader

ENV VERSION=v0.3.0

RUN apt update && \
    apt install -y gpg unzip curl && \
    cd /tmp && \
    curl -O -L https://github.com/LeSuisse/vault-gpg-plugin/releases/download/${VERSION}/linux_amd64.zip && \
    curl -O -L https://github.com/LeSuisse/vault-gpg-plugin/releases/download/${VERSION}/linux_amd64.zip.asc && \
    gpg --receive-keys FFCBD29F3AFED453AE4B9E321D40FBA29EB39616 && \
    gpg --verify linux_amd64.zip.asc && \
    unzip linux_amd64.zip

FROM alpine

COPY --from=downloader /tmp/vault-gpg-plugin /
